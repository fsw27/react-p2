import { Link } from "react-router-dom"
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem } from "reactstrap"

const NavigationComponent = () => {
    return (
        <>
            <Navbar expand={true} light className="bg-primary">
                <NavbarBrand>
                    React JS
                </NavbarBrand>
                <NavbarToggler/>
                <Collapse navbar>
                    <Nav navbar>
                        <NavItem>
                            <Link className="nav-link" to="/">Home</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link" to="/about">About</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link" to="/contact">Contact</Link>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        </>
    )
}

export default NavigationComponent

import { useEffect } from "react"
import { useNavigate } from "react-router-dom"

const About = () => {
    const navigate = useNavigate()

    const checkUser = () => {
        if (!localStorage.getItem("token")) {
            navigate("/login")
        }
    }

    useEffect(() => {
        checkUser()
    })

    return (
        <>
            <h1>About page</h1>
        </>
    )
}

export default About
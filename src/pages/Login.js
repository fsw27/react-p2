import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { Button, Input } from "reactstrap"

const mockFetch = (url, {body}) => {
    const { email, password } = body

    if (email === "sabrina@mail.com" && password === "123456") {
        return Promise.resolve({
            accessToken: "pura-pura"
        })
    }

    return Promise.reject("email atau password salah")
}

const Login = () => {
    const [loginData, setLoginData] = useState({
        email: "",
        password: "",
    })

    const navigate = useNavigate()

    const handleSubmit = e => {
        e.preventDefault()
        // console.log(loginData)
        mockFetch("https://localhost:8000/api/login", {
            method: "POST",
            body: loginData
        }).then(res => {
            localStorage.setItem("token", res.accessToken)
            navigate("/about")
        })
    }

    useEffect(() => {
        // componentDidMount
        console.log('componentDidMount')
    }, [])

    useEffect(() => {
        // componentDidUpdate
        console.log('componentDidUpdate')
    })
    
    useEffect(() => {
        return () => {
            // componentWillUnmount
            console.log('componentWillUnmount')
        }
    }, [])

    return (
        <div className="row mt-4">
            <form onSubmit={handleSubmit} className="col-md-4 offset-md-4">
                <Input type="email" 
                    onChange={e => setLoginData({...loginData, email: e.target.value})}
                    placeholder="Email"/>
                <Input type="password" 
                    onChange={e => setLoginData({...loginData, password: e.target.value})}
                    placeholder="Password"/>
                <Button color="primary" type="submit">Login</Button>
            </form>
        </div>
    )
}

export default Login
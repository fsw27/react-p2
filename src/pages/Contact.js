import { useState } from "react"
import { Input } from "reactstrap"

const Contact = () => {

    const [image, setImage] = useState()

    const fileChanged = (e) => {
        const file = e.target.files[0]
        console.log(file)
        const reader = new FileReader()
        reader.addEventListener("load", () => {
            setImage(reader.result)
        })

        reader.readAsDataURL(file)
    }

    return (<>
        <form>
            <Input type="file" onChange={fileChanged} accept=".png,.jpg,.jpeg,.gif" />
            {image && <img src={image} alt="uploaded"/>}
        </form>
    </>)
}

export default Contact
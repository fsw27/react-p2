import { useEffect } from "react"
import { useParams, useSearchParams } from "react-router-dom"

const Article = () => {

    const { id } = useParams()
    const [params, setParams] = useSearchParams()

    useEffect(() => {
        console.log(params.get("page"))
        console.log(params.get("perPage"))
    }, [params])

    const setQueryParameter = (e) => {
        const newParam = {
            page: params.get("page"),
            perPage: params.get("perPage"),
        }

        if (e.target.value) {
            newParam.q = e.target.value
        }
        setParams(newParam)
    }

    return (
        <>
            <h1>id = {id}</h1>
            <input type="search" onChange={setQueryParameter} />
        </>
    )
}

export default Article
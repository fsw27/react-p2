import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Card, CardBody } from "reactstrap"

const Home = () => {

    const [todoList, setTodoList] = useState([])

    useEffect(() => {
        // axios.get("https://jsonplaceholder.typicode.com/todos")
        //     .then(data => console.log(data))

        fetch('https://jsonplaceholder.typicode.com/todos')
            .then(res => res.json())
            .then(data => setTodoList([...data]))
    }, [])

    const renderTodoItem = (todo) => {
        return (
            <Card key={todo.id}>
                <CardBody>
                    {todo.title}
                </CardBody>
            </Card>
        )
    }

    return (
        <>
            <h1>Home page</h1>
            <Link to="/article/14">
                View article
            </Link>
            <Link to="/login">
                Login
            </Link>
            {todoList.map(renderTodoItem)}
        </>
    )
}

export default Home
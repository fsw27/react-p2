import { Route, Routes } from "react-router-dom"
import Home from "./pages/Home"
import About from "./pages/About"
import NavigationComponent from "./components/NavigationComponent"
import Article from "./pages/Article"
import Contact from "./pages/Contact"
import Login from "./pages/Login"

const Routing = () => {
    return (
        <>
            <NavigationComponent/>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/about" element={<About />} />
                <Route path="/article/:id" element={<Article />} />
                <Route path="/contact" element={<Contact />} />
                <Route path="/login" element={<Login />} />
            </Routes>
        </>
    )
}

export default Routing
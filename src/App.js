// import logo from './logo.svg';
// import './App.css';

import { useState } from "react";
import { Button, Form, Input } from "reactstrap";

const style = {
    formSubmit: {
        backgroundColor: "grey"
    }
}

function App() {
  const [value, setValue] = useState('default value')

  const handleSubmit = e => {
    console.log(value)
    e.preventDefault()
  }

  const handleChange = e => {
    setValue(e.target.value)
  }

  return (
    <>
        <Form onSubmit={handleSubmit} style={style.formSubmit}>
            <Input value={value} onChange={handleChange} />
            <Button type="submit" color="primary">Click me</Button>
        </Form>
    </>
  );
}

export default App;
